const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : 'localhost',
    user            : 'root',
    password        : 'root',
    database        : 'codecamp',
    //port            : 3306,
    socketPath         : '/Applications/MAMP/tmp/mysql/mysql.sock'

    
})

router.get('/instructors/find_all', async ctx => {
    const [rows] = await pool.query('SELECT * FROM instructors');
    console.log(rows)
    ctx.body = rows
    
})

router.get('/instructors/find_by_id/:id', async ctx => {
    const [rows] = await pool.query('SELECT * FROM instructors WHERE id = ?', [ctx.params.id]);
    ctx.body = rows
    
})

router.get('/courses/find_by_id/:id', async ctx => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE id = ?', [ctx.params.id]);
    // console.log(rows)
    ctx.body = rows
    
})


router.get('/course/find_by_price/:price', async ctx => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE price = ?', [ctx.params.price]);
    ctx.body = rows
  
})

app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3000)