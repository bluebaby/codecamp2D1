const {Employee} = require('./employee.js');
class PRO extends Employee {
    constructor(firstname, lastname, salary) {
      super(firstname, lastname, salary);
    }
    getSalary(){ // simulate public method
      return super.getSalary()*5;
    };
    hello() { // simulate public method
      console.log("Hi, Pros. "+this.firstname+"!");
    }
  }
  let pro = new PRO('Peter','Sudlor', 10000);
exports.PRO = PRO;