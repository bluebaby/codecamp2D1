const koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const render = require('koa-ejs')

const app = new koa()
const router = new Router()

render(app, {
    root: path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})
router.get('/', async ctx => {
    await ctx.render('index')
})

router.get('about', async ctx => {
    await ctx.render('about')
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)