const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const path = require('path')
const fs = require('fs')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : '127.0.0.1',
    user            : 'root',
    password        : 'root',
    port            :   3306,
    database        : 'homework11',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
})

router.get('/',async ctx => {
    // let employees = getEmployees();
    await ctx.render('homework11_1',employees);
    
})
let employees = getEmployees();
async function getEmployees() {
    
    let [rows] = await pool.query('SELECT * FROM users');
    return rows;
}

// app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})


app.listen(3000)