const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const pool  = mysql.createPool({
    connectionLimit : 10,
    host            : '127.0.0.1',
    user            : 'root',
    password        : 'root',
    port            :   3306,
    database        : 'codecamp',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
})

router.get('/instructor/find_all', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM instructors')
    ctx.body = rows;
    await next();
})

router.get('/instructor/find_by_id/', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM instructors WHERE id = ?' +ctx.params.id);
    // const [rows] = await pool.query('SELECT * FROM instructors WHERE id = "?"', [id])
    ctx.body = rows;
    await next();
})

router.get('/course/find_by_id/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE id = ?'+ctx.params.id);
    // const [rows] = await pool.query('SELECT * FROM courses WHERE id = "?"', [id])
    ctx.body = rows;
    await next();
})


router.get('/course/find_by_price/:price', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * FROM courses WHERE price = ?'+ctx.params.price);
    ctx.body = rows;
    await next();
})

app.use(router.routes())
app.use(router.allowedMethods())


app.listen(3000)