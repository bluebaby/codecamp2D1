let employees = {}
let newEmployees = {}

fetch('homework2_1.json').then(response => {
    return response.json();
 })
 .then(res => {
     setEmployee(res)
     return res;
 })
 .then(res => {
    newEmployees = addAdditionalFields(res)
    showTable()
    showTableNewEmployee()
 })
 .catch(error => {
    console.error('Error:', error);
 });
 
function addAdditionalFields(obj){
    let newObj = []
    for(let i = 0; i < obj.length ; i++){
        addYearSalary(obj[i])
        addNextSalary(obj[i])
        newObj[i] = {}  
        newObj[i].id = obj[i].id
        newObj[i].firstname = obj[i].firstname 
        newObj[i].lastname = obj[i].lastname 
        newObj[i].company = obj[i].company 
        newObj[i].salary = obj[i].salary 
    }
    return newObj
}
 
 function addYearSalary(row) {
    row.yearSalary = row.salary*12
} 

function addNextSalary(row)
{  
        let currentSalary = row.salary 
        row.nextSalary  = []
        for(j = 0; j < 3; j++)
        {
            let newSalary = j ? row.nextSalary[j-1]*10/100 + parseFloat(row.nextSalary[j-1]) : currentSalary
            row.nextSalary.push(parseFloat(newSalary))
        }
}

function setEmployee(obj){
    employees = obj;
}

function showTable(){
    for(let i = 0; i < employees.length; i++){
        $("#myTable").append(`
            <tr>
                <td> ${employees[i].id} </td>
                <td> ${employees[i].firstname} </td>
                <td> ${employees[i].lastname} </td>
                <td> ${employees[i].company} </td>
                <td> ${employees[i].salary} </td>
                <td> ${employees[i].yearSalary} </td>
                ${setSalary(employees[i].nextSalary)}
            </tr>
        `)

    }
}

function showTableNewEmployee(){
    for(let i = 0; i < newEmployees.length; i++){
        $("#newEmployee").append(`
            <tr>
                <td> ${newEmployees[i].id} </td>
                <td> ${newEmployees[i].firstname} </td>
                <td> ${newEmployees[i].lastname} </td>
                <td> ${newEmployees[i].company} </td>
                <td> ${newEmployees[i].salary} </td>
            </tr>
        `)

    }
}

function setSalary(arr)
{
    let txtSalary = "<td><ol>"
    for(let i = 0 ; i < arr.length ; i ++)
    {
        txtSalary += "<li>" + arr[i] + "</li>"
    }
    txtSalary += "</ol></td>"
    return txtSalary
}