CREATE DATABASE shopdb DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

create table department (
    id int auto_increment primary key,
    name varchar(50) not null,
    description varchar(200),
    bubget int(20) not null,
    create_at timestamp not null default now()
);

create table employee (
    id int auto_increment primary key,
    firstname varchar(50) not null,
    lastname varchar(50) not null,
    address varchar(200) not null,
    phone varchar(10) not null,
    email varchar(50) not null,
    depid int not null,
    salary int(10) not null,
    create_at timestamp not null default now(),
    foreign key (depid) references department (id)
    );

create table supplier(
    id int auto_increment primary key,
    company_name varchar(150) not null,
    address varchar(200) not null,
    phone varchar(12) not null,
    email varchar(50) not null,
    credit varchar(100) not null,
    create_at timestamp not null default now()
);

create table product(
    id int auto_increment primary key,
    pro_code varchar(10) not null  unique,
    name varchar(50) not null,
    description varchar(200) not null,
    price int(10) not null,
    quantity int(5) not null,
    supplier_id int not null,
    create_at timestamp not null default now(),
    foreign key (supplier_id) references supplier (id)
);

create table customer(
    id int auto_increment primary key,
    name varchar(50) not null,
    address varchar(200) not null,
    phone varchar(10) not null,
    email varchar(50) not null,
    payment varchar(100) not null,
    create_at timestamp not null default now()
);
create table orders(
    id int auto_increment primary key,
    order_no varchar(10) not null unique,
    cus_id int not null,
    emp_id int not null,
    product_code varchar(10) not null unique,
    price_unit int(10) not null,
    amount int(5) not null,
    discount int(10) not null,
    sum_unit int(15) not null,
    total_price int(20) not null,
    create_at timestamp not null default now(),
    foreign key (cus_id) references customer (id),
    foreign key (emp_id) references employee (id),
    foreign key (product_code) references product (pro_code)    
);