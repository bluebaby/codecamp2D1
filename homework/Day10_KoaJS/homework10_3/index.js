'use strict'
const Koa = require('Koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
let fs = require('fs')

const app = new Koa()
const router = new Router()

router.get('/', async ctx => {
    await ctx.render('index')
})

fs.readFile('./public/homework2_1.json', 'utf8', function(err, data){
    if (err)
    console.error(err);
    else{
    console.log(data);
    }
})


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)