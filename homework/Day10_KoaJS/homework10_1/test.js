const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

// router.get('/',ctx => {
//     ctx.body = 'this IDEX'
// })

// router.get('/about',ctx => {
//     ctx.body = '<img src = "images/1.jpg">'
// })
// app.use(serve(path.join(__dirname, 'public')))

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
   })
   
   router.get('/', async ctx => {
    await ctx.render('index')
   })
   
   router.get('/about', async ctx => {
    await ctx.render('about')
   })
   

app.use(router.routes())

app.listen(3000)

