const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

render(app,{
    root:path.join(__dirname,'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/',async ctx =>{
    await ctx.render('index')
})
router.get('/about',async ctx =>{
    await ctx.render('about')
})

// router.get('/about',ctx =>{
//     ctx.body = '<H1>this is about page</H1>>'
// })
// router.get('/video',ctx =>{
//     ctx.body = '<H1>this is video page</H1>'
// })
// router.get('/download',ctx =>{
//     ctx.body = '<H1>this is download page</H1>'
// })

app.use(serve(path.join(__dirname,'public')))
app.use(router.routes())
app.use(router.allowedMethods())

// app.use(ctx =>{
//     console.log(ctx.path)
//     if(ctx.path === '/about'){
//         ctx.body = 'this is about page about'
//     } else{
//             ctx.body = 'Hello'
//         }
    
//     // ctx.body = 'Hello World index'
// })

app.listen(3000)

