const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()



router.get('/',ctx =>{
    ctx.body('Hello index')
})



app.use(router.routes())
app.listen(3000)
