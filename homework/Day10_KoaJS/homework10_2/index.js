const Koa = require('Koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

// router.get('/index', async ctx => {
//     await ctx.render('index')
// })

router.get('/', async ctx => {
    await ctx.render('index')
})

router.get('/skill', async ctx => {
    await ctx.render('skill')
})

router.get('/contact', async ctx => {
    await ctx.render('contact')
})

router.get('/portfolio', async ctx => {
    await ctx.render('portfolio')
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)