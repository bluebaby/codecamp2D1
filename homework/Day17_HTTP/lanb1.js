const Koa = require('koa')
const koaBody = require('koa-body')

const app = new Koa()

app.use(koaBody())
app.use(ctx => {
console.log(ctx.request.body)
})

app.use(ctx => {
    if (!checkAuth()) {
      ctx.status = 401 // Unauthorized
      ctx.body = 'you must log in first'
    }
   })
   
app.listen(3000)
