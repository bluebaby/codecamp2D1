const mysql = require('mysql2/promise')

const pool = mysql.createPool({
	user: 'root',
	host: 'localhost',
	database: 'pikkanode'
})

module.exports = pool
