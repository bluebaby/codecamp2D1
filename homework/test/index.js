const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

router.get('/', async ctx => {
    await ctx.render('index')
})

router.get('/signin', async ctx =>{
    await ctx.render('signin')
})

router.get('/signup', async ctx => {
    await ctx.render('signup')
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(router.routes())
app.listen(8000)