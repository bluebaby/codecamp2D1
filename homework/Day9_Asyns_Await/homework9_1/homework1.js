'use stric'
let fs = require('fs')

async function robot() {
    try {
        let datahead = await readHead();
        let databody = await readBody();
        let dataleg = await readLeg();
        let datafeet = await readFeet();
        
        // let robot = await fs.appendFile('robot.txt', datahead + "\n" +databody+"\n"+dataleg+"\n"+datafeet, 'utf8', function (err) {
        // });
        let allrobot = datahead + "\n" +databody+"\n"+dataleg+"\n"+datafeet
        // console.log(allrobot)
        let datarobot = await writeFile(datahead);
        let robot =  await readFile();
    } catch (error) {
        console.error(error);
       
    }
}

robot() 

function readHead() {
    return new Promise(function (resolve, reject) {
        fs.readFile('head.txt', 'utf8', function (err, datahead) {
            if (err)
                reject(err)
            else
                resolve(datahead)
        })
    })
}
function readBody() {
    return new Promise(function (resolve, reject) {
        fs.readFile('body.txt', 'utf8', function (err, databody) {
            if (err)
                reject(err)
            else
                resolve(databody)
        })
    })
}
function readLeg() {
    return new Promise(function (resolve, reject) {
        fs.readFile('leg.txt', 'utf8', function (err, dataleg) {
            if (err)
                reject(err)
            else
                resolve(dataleg)
        })
    })
}
function readFeet() {
    return new Promise(function (resolve, reject) {
        fs.readFile('feet.txt', 'utf8', function (err, datafeet) {
            if (err)
                reject(err)
            else
                resolve(datafeet)
        })
    })
}

function writeFile() {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt','',  function (err) {
            if (err)
                reject(err)
            else
                resolve()
        })
    })
}



function readFile(){
    return new Promise(function(resolve, reject){
        fs.readFile('robot.txt','',function(err,robot){
            if(err)
            reject(err)
            else
            resolve(robot)
            console.log(robot)
        })
    })
}
