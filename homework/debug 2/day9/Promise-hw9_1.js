"use strict";
const fs = require('fs');

let p = [];

const numOfRead = 4;
let file = ['head', 'body', 'leg', 'fet'];
for (let i = 0; i < numOfRead; i++) {
    p[i] = new Promise(function (resolve, reject) {
        fs.readFile('node-homework8-1/' + file[i] + '.txt', 'utf8', function (err, data) {
            resolve(data);
        });
    });
}

function writeRobot(data) {
    return new Promise(function (resolve, reject) {
        let robotStr = data.reduce((sum, str) => {
            return sum + '\n' + str
        })
        console.log(robotStr);
        fs.writeFile('node-homework8-1/robot.txt', robotStr, 'utf8', function (err) {
            if (err)
                reject(err);
            else
                resolve("robot.txt Success!");
        });
    });
}

function createRobot() {
    try {
        let result = [];
        let data; 
        for (let i = 0; i < numOfRead; i++) {
            data = p[i];
            console.log(data);
            result.push(data);
        }
        console.log(result);
        data = writeRobot(result);
        console.log(data);
    } catch (error) {
        console.error(error);
    }
}
createRobot();

