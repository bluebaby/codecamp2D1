let jn1;

function addYearSalary(row) {
    row.yearSalary = row.salary * 12;
    return row;
}

const yr = 3
function addNextSalary(row) {
    for (j = 0; j < yr; j++) {
        row.nextSalary.push(row.nextSalary[j] * 110 / 100);
    }
    return row;
}
function addAdditionalFields(emp) {
    for (i = 0; i < Object.keys(emp).length; i++) {
        addYearSalary(emp[i]);
        addNextSalary(emp[i]);
    }
}

fetch('homework2_1.json').then(response => {
    if (response.ok) {
        return response.json();
    }
    throw new Error('Request failed!');
}, networkError => console.log(networkError.message)
).then(jsonResponse => {
    console.log(jsonResponse);
    addAdditionalFields(jsonResponse);

    console.log("\nAfter modifying =\n", jsonResponse);

    // Table Label
    $('body').append('<table id="tb"><tr id="trh"></tr></table>');

    for (x in jsonResponse) {
        $('#trh').append('<th>' + x + '</th>');
    }

    let i = 0;
    jsonResponse.forEach(arr => {
        $('#tb').append('<tr id="tr' + i + '"></tr>');
        for (x in arr) {
            if (x === 'nextSalary') {
                $('#tr' + i).append('<td><ol id="ol' + i + '"></ol></td>');
                for (j = 0; j < 3; j++) {
                    $('#ol' + i).append('<li>' + arr[x][j] + '</li>');
                }
            } else {
                $('#tr' + i).append('<td>' + arr[x] + '</td>');
            }
        }
        i++;
    });

});