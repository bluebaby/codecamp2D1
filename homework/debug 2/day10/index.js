const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router = new Router()

router.get('/',ctx => {
    ctx.render('index')
})

router.get('/about',ctx => {
    ctx.render('about')
})

app.use(router.routes())
app.use(router.allowedMethods())
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'html',
    cache: false
})

app.listen(3000)