const Koa = require('koa')
const cors = require('@koa/cors')
const koaBody = require('koa-body')
const serve = require('koa-static')
const render = require('koa-ejs')
const Router = require('koa-router')
const path = require('path')
const session = require('koa-session')


const app = new Koa()
const router = new Router()

const stripPrefix = async (ctx, next) => {
	if (!ctx.path.startsWith('/-')) {
		ctx.status = 404
		return
	}

	ctx.path = ctx.path.slice(2)
	await next()
}

const sessionStore = {}
const sessionConfig = {
  key: 'sess',
  maxAge: 1000 * 60 * 60,
  httpOnly: true,
  store: {
    get (key, maxAge, { rolling }) {
      return sessionStore[key]
    },
    set (key, sess, maxAge, { rolling }) {
      sessionStore[key] = sess
    },
    destroy (key) {
      delete sessionStore[key]
    }
  }
}

const flash = async (ctx, next) => { // Flash middleware
	if (!ctx.session) throw new Error('flash message required session')
	ctx.flash = ctx.session.flash
	delete ctx.session.flash
	await next()
   }
   
   function signinPostHandler (ctx) {
	if (!ctx.request.body.username) {
	  ctx.session.flash = { error: 'username required' }
	  return ctx.redirect('/signin')
	}
	// set authentication.
	ctx.redirect('/')
   }
   async function signinGetHandler (ctx) {
	const data = {
	  flash: ctx.flash
	}
	await ctx.render('signin', data)
   }


//   router.get('upload', async ctx => {
// 	  await ctx.render('upload')
//   })
   
   
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(session(sessionConfig, app))

app.use(serve(path.join(__dirname, 'public')))
app.use(koaBody({ multipart: true }))
app.use(flash)
app.use(require('./route'))
app.use(cors())
app.use(stripPrefix)


// app.use(suerve(path.join(process.cwd(), 'views')))
// app.use(serve('views'))

app.listen(8000)
