const mysql = require('mysql2/promise')

const pool = mysql.createPool({
	connectionLimit : 10,
	host            : '127.0.0.1',
    user            : 'root',
    password        : 'root',
    port            :   3306,
    database        : 'pikkanode',
    socketPath: '/Applications/MAMP/tmp/mysql/mysql.sock'
	
})

module.exports = pool
