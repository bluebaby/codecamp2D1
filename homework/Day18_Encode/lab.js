const bcrypt = require('bcrypt')
const password = 'superhero'

async function hashPassword () {
  const hashedPassword = await bcrypt.hash(password, 5)
  console.log('hashed password: ', hashedPassword)
}

async function comparePassword (input) {
  const hashedPassword = '$2b$05$sZofQW0wcw5t542AuMOsau1Y/UUgKCN57QqoqvgNtlq441/T3lozS'
  const same = await bcrypt.compare(input, hashedPassword)
  if (!same) {
    console.log(input, ' is wrong')
  } else {
    console.log(input, ' is correct')
  }
}

hashPassword()

comparePassword('superhero')
comparePassword('superman')
