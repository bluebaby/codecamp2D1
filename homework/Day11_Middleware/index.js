const Koa = require('Koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

app.use(serve(path.join(__dirname, 'public')))

router.get('/', async (ctx, next) => {
    ctx.myVarible = 'forbidden';
    await next();
})

async function myMiddleware(ctx, next) {
    if (ctx.myVarible == 'forbidden')
        ctx.body = 'Access Denied';
    else
        await next();
}

app.use(myMiddleware)
app.use(myMiddleware)
app.use(router.routes())
app.use(myMiddleware)
app.use(myMiddleware)
app.use(router.allowedMethods())


app.listen(3000)
